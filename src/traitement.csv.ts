import csv from 'csv-parser';
import * as fs from 'fs';
import Article from './models/article.model';


// Fonction pour lire le fichier CSV et insérer les données dans la base de données
const importCSV = async (filePath: string): Promise<void> => {
    const results: any[] = [];

    // Lire le fichier CSV ligne par ligne
    fs.createReadStream(filePath)
        .pipe(csv())
        .on('data', (data: any) => {
            // Traitement des données si nécessaire
            results.push(data); // Ajouter les données à un tableau
        })
        .on('end', async () => {
            // Insérer les données dans la base de données
            try {
                await Article.bulkCreate(results); // Insérer les données dans la table "Article"
                console.log('Données insérées avec succès !');
            } catch (error) {
                console.error('Erreur lors de l\'insertion des données :', error);
            }
        });
};

// Appel de la fonction avec le chemin du fichier CSV
importCSV("./fichier.csv/article.csv");
export default importCSV;