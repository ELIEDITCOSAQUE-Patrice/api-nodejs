import express, { Router } from "express";
import { Request, Response } from "express";
import { getArticles, getArticleById, postArticles, editArticle, deleteArticle } from "../controllers/article.controller";

const router: Router = express.Router();

router.get("/", getArticles);
router.get("/:id", getArticleById);
router.post("/", postArticles);
router.put("/:id", editArticle);
router.delete("/:id", deleteArticle);

export default router;