import { DataTypes, Model } from "sequelize";
import {sequelize} from "../config/database";
import Role from "./role.model";
import ArticleAttributes from "../interfaces/article.interface";
import User from "./user.model";

class Article extends Model<ArticleAttributes> implements ArticleAttributes {
  public id?: number;
  public nom!: string;
  public description!: string;
  public taille!: string;
  public prix!: number;
  public statut!: string;
  public date_publication!:Date;
  public photo?: string;

  static initModel = () => {
    Article.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        nom: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        taille: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        prix: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        statut: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        date_publication: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        photo: {
          type: DataTypes.STRING,
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "articles",
      }
    );
  };
  static associate() {
    Article.belongsTo(User);
  }
}
export default Article;