import { Request, Response } from 'express';
import ArticleModel from '../models/article.model';

/**
 * Récupère tous les articles.
 * @param {Request} req - L'objet de requête HTTP.
 * @param {Response} res - L'objet de réponse HTTP.
 * @returns {Promise<void>} Une promesse résolue une fois que les articles sont récupérés et renvoyés dans la réponse.
 */
export const getArticles = async (req: Request, res: Response): Promise<void> => {
    try {
      const articles = await ArticleModel.findAll();
      res.status(200).json(articles);
    } catch (error) {
      console.error("Erreur lors de la récupération des articles :", error);
      res.status(500).json({ message: "Erreur lors de la récupération des articles" });
    }
  };

/**
 * @param {Request} req
 * @param {Response} res
 */
export const getArticleById = async (req: Request, res: Response): Promise<void> => {
  const articleId: string = req.params.id; // Supposons que l'identifiant de l'article est passé en tant que paramètre dans l'URL
  try {
    const article = await ArticleModel.findByPk(articleId); // Utilisez la méthode findByPk pour récupérer un article par son identifiant
    if (!article) {
        res.status(404).json({ message: "Article non trouvé" });
      } else {
        res.status(200).json(article);
      }
    } catch (error) {
      console.error("Erreur lors de la récupération de l'article :", error);
      res.status(500).json({ message: "Erreur lors de la récupération de l'article" });
    }
  };

/**
 * @param {Request} req
 * @param {Response} res
 */
export const postArticles = async (req: Request, res: Response): Promise<void> => {
  if (!req.body.message) {
    res.status(400).json({ message: "Merci d'ajouter un article" });
  }

  const article = await ArticleModel.create({
    nom: req.body.nom,
    description: req.body.description,
    taille: req.body.taille,
    prix: req.body.prix,
    statut: req.body.statut,
    date_publication: req.body.date_publication,
    photo: req.body.photo,
    
  });
  res.status(200).json(article);
};

/**
 * @param {Request} req
 * @param {Response} res
 */
export const editArticle = async (req: Request, res: Response): Promise<void> => {
    const articleId: string = req.params.id;
    try {
      const [updatedRowsCount, updatedRows] = await ArticleModel.update(req.body, {
        where: { id: articleId },
        returning: true
      });
      if (updatedRowsCount === 0) {
        res.status(404).json({ message: "Article non trouvé" });
      } else {
        res.status(200).json(updatedRows[0]);
      }
    } catch (error) {
      console.error("Erreur lors de la mise à jour de l'article :", error);
      res.status(500).json({ message: "Erreur lors de la mise à jour de l'article" });
    }
  };

/**
 * @param {Request} req
 * @param {Response} res
 */
export const deleteArticle = async (req: Request, res: Response): Promise<void> => {
    const articleId: string = req.params.id;
    try {
      const deletedRowCount = await ArticleModel.destroy({
        where: { id: articleId }
      });
      if (deletedRowCount === 0) {
        res.status(404).json({ message: "Article non trouvé" });
      } else {
        res.status(200).json({ message: "Article supprimé avec succès" });
      }
    } catch (error) {
      console.error("Erreur lors de la suppression de l'article :", error);
      res.status(500).json({ message: "Erreur lors de la suppression de l'article" });
    }
  };