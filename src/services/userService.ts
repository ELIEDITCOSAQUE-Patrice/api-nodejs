import UserModel from '../models/user.model';
import Role from '../models/role.model';
import Article from './../models/article.model';

class UserService {
    static async getUsersWithDetails(): Promise<any[]> {
     const usersWithDetails = await UserModel.findAll({
          include: [
            {
              model: Role,
              attributes: ['nom'],
            },
            {
              model: Article,
              attributes: ['nom', 'description', 'taille', 'prix'],
              required: false,
            },
          ],
          attributes: [ 'nom', 'prenom', 'email',]
        });
        return usersWithDetails;
      }
}
export default UserService;