interface ArticleAttributes {
    id?: number;
    nom: string;
    description: string;
    taille: string;
    prix:number;
    statut: string;
    date_publication: Date;
    photo?: string;
   
  }
  
  export default ArticleAttributes;