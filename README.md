- npm init : initialisez votre projet Node.js
- npm install : installez les dépendances du projet

## 2. Installer les dépendances

- npm install express --save : installez express
- npm install mysql2 :
- npm init -y et npm install sequelize mariadb :
- npm install dotenv
- npm install nodemon --save-dev
- npm install typescript --save-dev
- npm install @types/node @types/express @types/sequelize

## 3. Lancement

- npm start : pour lancer le server et faire la connexion de la base de données
- npm start --"delete" : pour lancer le server, faire la connexion de la base de données et la synchronisation des tables
